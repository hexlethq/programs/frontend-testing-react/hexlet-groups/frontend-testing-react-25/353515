let src;
let target;
let result;

beforeEach(() => {
  src = { k: 'v', b: 'b' };
  target = { k: 'v2', a: 'a' };
  result = Object.assign(target, src);
});

test('main', () => {
  expect.hasAssertions();

  // BEGIN
  const expected = { k: 'v', a: 'a', b: 'b' };
  expect(result).toEqual(expected);
  // END
});

test('mutation target object', () => {
  expect(result).toEqual(target);
});
